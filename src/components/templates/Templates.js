import React, {Component} from 'react';

import "./templates.css";

export class Heading extends Component {

  render() {
    let headingStyle = {
      fontSize: "20pt",
      textAlign: "left",
    };

    return (
      <span style={{...this.props.style, ...headingStyle}} onClick={this.props.onClick}>
                {this.props.children} </span>
    );
  }
}

export class TableHead extends Component {

  render() {
    let tableHeadStyle = {
      fontSize: "10pt",
      marginBottom: "24px"
    };

    return (
      <thead className="text-center" style={tableHeadStyle}>{this.props.children}</thead>
    );
  }
}

export class TableBody extends Component {

  render() {
    let tableBodyStyle = {
      overflow: "scroll"
    };

    return (
      <tbody style={tableBodyStyle}>{this.props.children}</tbody>
    );
  }
}

export class FormRow extends Component {

  render() {
    return (<div style={{display: "flex", flexWrap: "nowrap", width: "100%", margin: "8px auto"}}>
      <label style={{
        width: "33%",
        verticalAlign: "middle",
        fontSize: "12pt",
        textAlign: "left",
        fontWeight: "bold",
        margin: "auto 0"
      }}>{this.props.label}</label>

      <div id={this.props.label} style={{display: "flex", flexWrap: "nowrap", width: "67%"}}>{this.props.children}</div>
    </div>);
  }
}

export class Dropdown extends Component {

  render() {
    let viewWidth = (this.props.width) || "100%";

    let dropdownStyle = {
      width: viewWidth,
    };

    return (
      <select className="form-input"
              name={this.props.name}
              style={{...dropdownStyle, ...this.props.style}}
              defaultValue={this.props.defaultValue || ""}
              required={this.props.required}
              onChange={this.props.onChange}>
        <option value="" disabled hidden>{this.props.placeholder}</option>
        {this.props.children}
      </select>
    );
  }
}

export class InputArea extends Component {

  render() {
    let inputStyle = {
      width: this.props.width || "100%",
    };

    return (
      <input name={this.props.name}
             className="form-input"
             style={inputStyle}
             type={this.props.type}
             placeholder={this.props.placeholder}
             required={this.props.required}
             onChange={this.props.onChange}
             max={this.props.max}
             min={this.props.min}
             defaultValue={this.props.value}
      />
    );
  }
}

export class SingleHolidayCard extends Component {
  render() {
    let cardStyle = {
      boxShadow: "0 2px 6px 0 rgba(0,0,0,0.16)",
      maxWidth: "",
      display: "flex",
      flexDirection: "column",
      padding: "12px 24px"
    };

    let nameStyle = {
      fontSize: "12pt",
      fontWeight: "bold",
      marginBottom: "4px"
    };

    let dateStyle = {
      fontSize: "10pt",
      marginBottom: "8px"
    };

    return (
      <div style={cardStyle}>

        <div style={nameStyle}>{this.props.holiday.name}</div>
        <span style={dateStyle}>{this.props.holiday.date}</span>
      </div>);
  }
}

export class SingleWeekDay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      holiday: this.props.holiday
    };

    this.toggleDayState = this.toggleDayState.bind(this)
  }

  weekdayStyle = function () {

    let commonStyle = {
      display: "flex",
      height: "36px",
      width: "36px",
      borderRadius: "50%",
      justifyContent: "center",
      alignItems: "center",
      fontSize: "12pt",
      cursor: "pointer",
      color: "currentColor",
      margin: "auto"
    };

    let holidayStyle = {
      color: "white", backgroundColor: "#FF7A7A"
    };

    let workDayStyle = {
      color: "#505050", border: "1px solid #505050"
    };

    return (this.state.holiday) ? {...commonStyle, ...holidayStyle} : {...commonStyle, ...workDayStyle};
  };

  toggleDayState(e) {
    this.setState((oldState) => ({
      holiday: !oldState.holiday
    }));
  }

  render() {
    return (
      <span style={this.weekdayStyle()} onClick={this.toggleDayState}>{this.props.children}</span>
    );
  }
}

export class SubmitButton extends Component {

  render() {
    return (
      <input type="submit" value={this.props.value}/>
    );
  }
}