import React from 'react';
import { Nav, NavItem } from 'reactstrap';

import profileImage from '../../../resources/images/person.svg';
import './styles.css';

class NavBar extends React.Component {

    render() {
        let handleSelect = (event) => {
        };

        return (
            <Nav className="navBar" bsStyle="pills" stacked onSelect={handleSelect}>
                
                <NavItem eventKey={1} href="/profile">
                    Student Profile
                    </NavItem>
                <NavItem eventKey={2}>
                    Score Sheet
                    </NavItem>
                <NavItem eventKey={3} disabled>
                    NavItem 3
                    </NavItem>
            </Nav>

        );
    }
}

class UserCard extends React.Component{
    render(){
        return (
            <div className='user-card'>            
                <img src={profileImage} alt="" />
                <span className="name">Name</span>
            </div>
        );
    }
}
export default NavBar;