import React from 'react';
import {InputArea, SubmitButton, Dropdown} from '../templates/Templates';

import logoIcon from '../../resources/images/logo.svg';
import './styles.css';

class LoginPage extends React.Component {
    render() {
      return (
        <div className="form-parent">
  
          <form className="login-form">

            <img class="logo-icon" src={logoIcon} alt=""/>
            <InputArea placeholder="School Name" width="240px" type="text"/>
            <InputArea placeholder="Username" width="240px" type="username"/>
            <InputArea placeholder="Password" width="240px" type="password"/>
            
            <SubmitButton value="Login"/>
  
            <a id="forgot-pw" href="">Forgot Password</a>
          </form>
        </div>
      );
    }
  }
  
  export default LoginPage;