import React from 'react';
import { Route, RouteProps, Switch } from 'react-router-dom';
import LoginPage from './components/login'
import Dashboard from './components/dashboard';

import './App.css';

class App extends React.Component {
  render() {
    const appBody = (<Switch>
      <Route path="/login/" render={
        (props) => <LoginPage {...props} />
      } />
      <Route path="/" render={
        (props) => <Dashboard {...props} />
      } />
    </Switch>);

    return (
      <div className="App">
        <div id="App-body" className="App-body">
          {appBody}
        </div>
      </div>
    );
  }
}

export default App;